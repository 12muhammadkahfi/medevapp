<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('ip_addr','NetworkController@ip_add');
Route::get('memory','NetworkController@memory_usage');
Route::get('memory_usage','NetworkController@memory_size_usage');
Route::get('larinfo_device','NetworkController@larinfo_all');
Route::post('add','InputDBController@insert_data');
Route::post('create_table', 'InputDBController@adding_table');
Route::post('case_table', 'InputDBController@adding_table_case');
Route::post('add_table','InputDBController@create_table_arr');
Route::get('a', function () {
    echo "aaaaa";
});
// Route::middleware('auth:api')->get('/user', function () {
//     return $request->user();
// });

