<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\EncController as Counter;
class HeaderController extends Controller
{
    //
    static function tes_set(){
        return Counter::header_validation();
    }
    static function set_header_taken(){
        if(!isset($_SERVER['PHP_AUTH_USER'])){
            header('WWW-Authentication: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            die('Not authorized');
        }
        $user_take = $_SERVER['PHP_AUTH_USER'];
        $pass_take = $_SERVER['PHP_AUTH_PW'];
        if($pass_take !=  Counter::header_validation()){
            header('WWW-Authentication: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            die ('Not authorized');
        }
    }
}
