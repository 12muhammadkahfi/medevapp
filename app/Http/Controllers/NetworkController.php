<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Matriphe\Larinfo\Larinfo;
class NetworkController extends Controller
{
    public function ip_add(Request $request){
        if($request->isMethod('get')){
            $ipaddress = getmypid();
            $arr = [
                'pid'=> $ipaddress
            ];
            return response()->json(compact('arr'), 200);
        }else{
            return response()->json('Wrong Method', 201);
        }
    }
    public function memory_usage() { 
        $mem_usage = memory_get_usage(true);   
        if ($mem_usage < 1024) 
              $tex =  $mem_usage." bytes"; 
        elseif ($mem_usage < 1048576) 
            $tex =  round($mem_usage/1024,2)." kilobytes"; 
        else 
            $text = round($mem_usage/1048576,2)." megabytes";
        $arr = [
            'byte' => $mem_usage ." bytes(Byte)",
            'kilobytes' => round($mem_usage/1024,2)." kilobytes(KB)",
            'megabytes' => round($mem_usage/1048576,2)." megabytes(MB)",
        ];
        return response()->json(compact('arr'), 200);
    }
    function convert($size)
    {
        $unit=array('b','kb','mb','gb','tb','pb');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }
    public function memory_size_usage(){
        $arr = [
            'usage' => $this->convert(memory_get_usage(true)),
        ];
        return response()->json(compact('arr'), 200);
    }
    public function larinfo_all(Request $request){
        $mem_usage = memory_get_usage(true); 
        $ip = "Last modified: " . date("F d Y H:i:s.", getlastmod());
        $arr = [
            'server'=> $request->server(),
            'memory_bytes'=> $mem_usage." bytes",
            'memory_kilobytes' => round($mem_usage/1024,2)." KB",
            'memory_megabytes' => round($mem_usage/1048576,2)." MB",
            'ip' => $request->ip(),
            'ip_client' => $request->ips(),
            'capture' => $request->capture(),
            'mehode'=>$request->method(),
            'url' => $request->url(),
            'fullUrl' => $request->fullUrl(),
            'allFiles' => $request->allFiles(),
            'bearerToken' => $request->bearerToken(),
            'fingerprint' => $request->fingerprint(),
            'last_update'=> $ip,
        ]; 
        return response()->json(compact('arr'), 200);
    }
}
