<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use DB;
use Config;
class DatabaseController extends Controller
{
    function __construct()
    {
        
    }
    static function table_create($table_name, $fields = [])
    {
        if (!Schema::hasTable($table_name)){
            Schema::create($table_name, function (Blueprint $table) use ($fields, $table_name){
                $table->increments('id');
                if(count($fields)>0){
                    foreach ($fields as $field) {
                        # code...
                        $table->{$field['type']}($field['name']);
                    }
                }
                $table->timestamps();
            });
            echo response()->json(['message'=> 'Was Succesed Create Table'], 200);
        }
        echo response()->json(['message'=> 'Table Already'], 400);
    }
    static function dev_remove($table_name){
        if(Schema::dropIfExists($table_name)){
           return true;
        }else{
            return false;
        }
    }
    static function table_add($table_name, $fields = []){
        if(Schema::hasTable($table_name)){
            Schema::table($table_name, function (Blueprint $table) use ($fields, $table_name){
                if(count($fields)>0){
                    foreach ($fields as $field) {
                        # code...
                        $table->{$field['type']}($field['name']);
                    }
                }
            });
            echo response()->json(['message'=>'Was Succes ed  Create Table'],200);
        }
        echo response()->json(['message'=>'Was Failed Create Table'],400);
    }
    static function list_table(){
        $tables = DB::select('SHOW TABLES');
        $tables = array_map('current',$tables); 
        return $tables;
    }
    static function list_db(){
        $n_ba = DB::select('SHOW databases');
        $table = array_map('current',$n_ba);
        return $table;
    }
    static function complete_db(Request $request){
        $attr = array();
        $start = microtime(true);
        $executionTime = round((microtime(true) - $start) * 1000, 3);
        $n_ba = DB::select('SHOW databases');
        $db_a = array_map('current', $n_ba);
        if(count($n_ba)>0){
            foreach ($db_a as $key => $value) {
                # code...
                $table = DB::select('SHOW tables FROM '.$value);
                $tables = array_map('current', $table);
                $arr = [
                    'time_to_execution'=> $executionTime, 
                    'fingerprint'=>$request->fingerprint(),
                    'count'=> count($tables),
                    'database'=>$value,
                    'table' => $tables
                ];
                 array_push($attr, $arr);
            }  
        }
        return $attr;
    }
    static function show_field($key)
    {
        $table = DB::select('SHOW tables FROM'. $key);
        $tables = array_map('current', $table);
        return $table;

    }


    //
}
