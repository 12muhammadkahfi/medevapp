<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\DatabaseController as DB_App;
use DB;
class InputDBController extends Controller
{
    function __construct()
    {
        
    }
    public function adding_field(Request $request)
    {
         if($request->isMethod('post')){
            if(isset($request->table)){
                $arr = [
                    [
                     'name' => $request->input('name'),
                     'type' => $request->input('type')
                    ]
                ];
                DB_App::table_add($request->input('table'),$arr);
            }
        }
    }
    public function adding_table(Request $request)
    {
        $method = $request->method();
        $attr =array();
        if ($request->isMethod('post')) {
            $arr = [
                ['name'=>$request->input('field'), 'type'=>$request->input('type')]
            ];
            DB_App::table_create($request->input('table'), $arr);
        }else{
            return response()->json(['message'=> 'Was Failed Method Using'],400);
        }
    }
    public function adding_table_case(Request $request)
    {
        $method = $request->method();
        $attr =array();
        if ($request->isMethod('post')) {
            switch ($request->input('count')) {
                case '1':
                    # code...
                    $arr = [    
                            ['name'=>$request->input('field'),'type'=>$request->input('type')]
                           ];
                    break;
                case '2':
                    $arr = [
                            ['name'=>$request->input('field'),'type'=>$request->input('type')],
                            ['name'=>$request->input('field2'),'type'=>$request->input('type2')]
                           ];
                    break;
                case '3':
                    $arr = [
                            ['name'=>$request->input('field'),'type'=>$request->input('type')],
                            ['name'=>$request->input('field2'),'type'=>$request->input('type2')],
                            ['name'=>$request->input('field3'),'type'=>$request->input('type3')]
                           ];
                    break;
                case '4':
                    $arr = [
                            ['name'=>$request->input('field'),'type'=>$request->input('type')],
                            ['name'=>$request->input('field2'),'type'=>$request->input('type2')],
                            ['name'=>$request->input('field3'),'type'=>$request->input('type3')],
                            ['name'=>$request->input('field4'),'type'=>$request->input('type4')]
                           ];
                    break;
                case '5':
                    $arr = [
                            ['name'=>$request->input('field'),'type'=>$request->input('type')],
                            ['name'=>$request->input('field2'),'type'=>$request->input('type2')],
                            ['name'=>$request->input('field3'),'type'=>$request->input('type3')],
                            ['name'=>$request->input('field4'),'type'=>$request->input('type4')],
                            ['name'=>$request->input('field5'),'type'=>$request->input('type5')],
                           ];
                    break;
                default:
                    # code...
                    return response()->json(['message'=> 'Numeric Not Found']);
                    return false;
                    break;
            }
            DB_App::table_create($request->input('table'), $arr);
        }else{
        return response()->json(['message'=> 'Was Failed Method Using'],400);
        }
    }
    public function remove_table(Request $request){
        $method = $request->method();
        if($request->isMethod('post')){
            if($request->input('table')){
            // if(isset($request->input('table'))){
                $data = DB_App::dev_remove($request->table);
                if($data){
                    return response()->json(['message'=>'Succesed Remove'],200);
                }else{
                    return response()->json(['message'=>'Failed Remove'],200);
                }
            }else{
                return response()->json(['message'=>'Your attribut did not find'],204);
            }
        }else{
            return response()->json(['message'=>'Your Request Not Match'],400);
        }
    }
    public function list_table(Request $request){
        $value = DB_App::list_table();
        if(count($value)>0){
            $arr = [
                'execution'=>microtime(true),
                'status'=>'success',
                'fingerprint'=>$request->fingerprint(),
                'count'=>count($value),
                'nama_table'=> $value
            ];
            return response()->json(compact('arr'),200);
        }
    }
    public function list_database(Request $request){
        $value = DB_App::list_db();
        if(count($value)>0){
            $arr = [
                'execution'=>microtime(true),
                'status'=>'success',
                'fingerprint' => $request->fingerprint(),
                'count'=>count($value),
                'nama_database'=> $value
            ];
            return response()->json(compact('arr'), 200);
        }
    }
    public function list_db_table(Request $request){
        $value = DB_App::show_field($request->input('table'));
        if(count($value)>0){
            $data = [
                'execution' => microtime(true),
                'status' => 'success',
                'fingerprint' => $request->fingerprint(),
                'count' => count($value),
                'data' => $value
            ];
            return response()->json(compact('data'),200);
        }else{
            return response()->json(compact('Was Wrong Field'),204);
        }
    }
    public function insert_data(Request $request){
        $data_attr = array();
        $data_value = array();
        $data_x = array();
        $data2 = array_keys($request->input());
        $value2 = array_keys($request->input());
        while ($key_name_2 = current($value2)) {
            if ($key_name_2 == 'table') {
                $attr_val = $request->input($key_name_2);
                array_push($data_value, $attr_val);
            }
            next($value2);
        }
        $flush_2 = $data_value[0];   
        while ($key_name = current($data2)) {
            if ($key_name != 'table') { 
                $data = $key_name; 
                $val_x = $request->input($key_name);
                array_push($data_attr, $data);
                array_push($data_x, $val_x);
            }
            next($data2);
        }
        DB::insert("insert into ".$flush_2." (".implode(",",$data_attr).") values ('".implode("','",$data_x)."') ");
    }
    public function create_table_arr(Request $request){
        $method = $request->method();
        $data_value = array();
        $attr =array();
         if ($request->isMethod('post')) {
            $arr = array(
                'name'=>$request->input('field.*'),
                'type'=>$request->input('type.*')
            );
            // $value = array_keys($request->input());
            // while ($key_name = current($value)) {
            //     if ($key_name == 'table') {
            //         $attr_val = $request->input($key_name);
            //         array_push($data_value, $attr_val);
            //     }
            //     next($value);
            // }}
            // return response()->json(compact('arr'),200);
            DB_App::table_create($request->input('table'), $attr);
        }elseif($request->isMethod('get')){
            return response()->json(['message'=> 'Was Failed Method Using'],400);
         }
    }
    //
}
